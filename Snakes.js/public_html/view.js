var View = function (board) {
    this.Q = 10; // the ratio box-coordinate / pixels
    this.pixelSize = board.size * this.Q;
    this.canvas = this.canvas || document.createElement("canvas");
    this.canvas.width = board.size * this.Q;
    this.canvas.height = board.size * this.Q;
    this.context = this.canvas.getContext("2d");
    document.body.appendChild(this.canvas);

};

View.prototype.update = function (snake, level) {
    this.clear();
    this.drawSnake(snake);
    this.drawObstacles(level);
    this.drawPills(level);
};

View.prototype.clear = function () {
    this.context.fillStyle = "#d9ffb3";
    this.context.fillRect(0, 0, this.pixelSize, this.pixelSize);
    this.context.strokeStyle = "#000";
    this.context.strokeRect(0, 0, this.pixelSize, this.pixelSize);
};

View.prototype.drawBox = function (box, strokeColor, fillColor) {
    var pixelX = box.x * this.Q;
    var pixelY = box.y * this.Q;
    this.context.fillStyle = fillColor; 
    this.context.strokeStyle = strokeColor;
    this.context.fillRect(pixelX, pixelY, this.Q, this.Q);
    this.context.strokeRect(pixelX, pixelY, this.Q, this.Q);
};

View.prototype.drawSnake = function (snake) {
    var body = snake.snakeBody;
    for (var i = 0; i < body.length; i++) {
        this.drawBox(body[i], "#194d19", "#cccc00");
    }
};

View.prototype.drawObstacles = function (level) {
    var obstacles = level.obstacles;
    for (var i = 0; i < obstacles.length; i++) {
        this.drawBox(obstacles[i], "#000", "#adad85");
    }
};

View.prototype.drawPills = function (level) {
    var pills = level.pills;
    for (var i = 0; i < pills.length; i++) {
        this.drawBox(pills[i].box, "#e6e600", pills[i].getColor());
    }
};
