var Box = function (x, y) {
    this.x = x;
    this.y = y;
};

Box.prototype.equals = function (other) {
    if (other.x === this.x && other.y === this.y){
        return true;
    }
    return false;
};

Box.prototype.toString = function () {
    return "[" + this.x + "," + this.y + "]";
};

