var Game = function () {
    this.stopped = true;
    this.delay = 100;
    this.START_BOX = new Box(0, 0);
    this.START_LEN = 3;
    this.board = new Board(50);
    this.view = new View(this.board);
    this.levelNumber = 1;
};


Game.prototype.start = function () {
    // remove previously registered keypresshandler
    window.removeEventListener("keydown", this.keyPressHandler); 
    this.stopped = true;
    this.level = new Level(this.levelNumber);
    this.snake = new Snake(this.START_LEN, this.START_BOX);
    this.keyPressHandler = new KeyPressHandler(this.snake, this);
    this.view.update(this.snake, this.level);
    window.addEventListener("keydown", this.keyPressHandler);
};


Game.prototype.play = function () {
    console.log("play");
    if (this.clash()) {
        console.log("clash");
        this.finish();
        return;
    } else {
        console.log("move");
        console.log(this.stopped);
        this.snake.move();
        this.swallow();
    }
    // update the view:
    this.view.update(this.snake, this.level);

    if (this.level.pills.length === 0) {
        this.stopped = true;
        setTimeout(this.levelFinished.bind(this), 100); // wait a little to let the view update
    } else if (!this.stopped) {
        setTimeout(this.play.bind(this), this.delay);
    }
};

Game.prototype.finish = function () {
    alert("Game finished!");
    this.start();
};

Game.prototype.levelFinished = function () {
    if (this.level.lastLevel) {
        alert("Great!! You have come to the end of the game!");
        this.levelNumber = 1;
    } else {
        alert("Congratulations! You have finished the level number " + this.levelNumber + ".");
        this.levelNumber++;
    }
    this.start();
};

Game.prototype.clash = function () {
    var barriers = this.board.borders
            .concat(this.snake.snakeBody)
            .concat(this.level.obstacles);
    // barriers are borders of the board plus obstacles from the level
    // plus the snake itself
    return barriers.some(x => this.snake.getForehead().equals(x));
};

Game.prototype.swallow = function () {
    var pills = this.level.pills;
    for (var i = 0; i < pills.length; i++) {
        if (pills[i].box.equals(this.snake.head())) {
            this.snake.grow(pills[i].power);
            this.level.removePill(pills[i]);
        }
    }
};


