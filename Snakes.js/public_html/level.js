var Level = function (number) {
    this.lastLevel = false;
    this.obstacles = [];
    this.pills = [];
    this.init(number);
};

Level.prototype.init = function (number) {
    switch (number) {
        case 1:
            // ***** LEVEL 1 **************
            this.createHorizontalLine(10, 40, 10);
            this.createHorizontalLine(10, 40, 40);
            this.createVerticalLine(10, 12, 38);
            this.createVerticalLine(40, 12, 38);
            
            this.createHorizontalLine(20, 30, 20);
            this.createHorizontalLine(20, 30, 30);
            this.createVerticalLine(20, 22, 28);
            this.createVerticalLine(30, 22, 28);
            this.addPill(15, 25, 4);
            this.addPill(25, 15, 4);
            this.addPill(25, 25, 4);
            this.addPill(35, 25, 4);
            this.addPill(25, 35, 4);
            break;
        case 2:
            // ***** LEVEL 2 ***************
            for (var i = 0; i < 2 * Math.PI; i += 0.05) {
                for (var j = 20; j >= 10; j-=10) {
                    var x = Math.floor(j * Math.sin(i) + 24);
                    var y = Math.floor(j * Math.cos(i) + 24);
                    this.createObstacle(x, y);
                }
            }
            this.removeObstacle(4, 24);
            this.removeObstacle(43, 24);
            this.removeObstacle(14, 24);
            this.addPill(24, 24, 5);
            this.addPill(19, 19, 4);
            this.addPill(29, 29, 4);
            this.addPill(15, 15, 3);
            this.addPill(32, 32, 3);
            this.addPill(36, 36, 2);
            this.addPill(11, 11, 2);
            this.lastLevel = true;
            break;
        default:
            break;
    }
};

Level.prototype.createObstacle = function (x, y) {
    this.obstacles.push(new Box(x, y));
};

Level.prototype.removeObstacle = function (x, y) {
    for (var i = 0; i < this.obstacles.length; i++) {
        var obstacle = this.obstacles[i];
        if (obstacle.x === x && obstacle.y === y) {
            this.obstacles.splice(i, 1);
        }
    }
};

Level.prototype.createHorizontalLine = function (startX, endX, y) {
    for (var i = startX; i <= endX; i++) {
        this.createObstacle(i, y);
    }
};

Level.prototype.createVerticalLine = function (x, startY, endY) {
    for (var i = startY; i <= endY; i++) {
        this.createObstacle(x, i);
    }
};


Level.prototype.addPill = function (x, y, power) {
    this.pills.push(new Pill(x, y, power));
};

Level.prototype.removePill = function (pill) {
    for (var i = 0; i < this.pills.length; i++) {
        if (this.pills[i].box.equals(pill.box)) {
            this.pills.splice(i, 1);
        }
    }
};



