var Board = function (size) {
    this.size = size;
    this.borders = [];
    this.init();
};

Board.prototype.init = function () {
    for (var i = 0; i < this.size; i++) {
        this.borders.push(new Box(-1, i));
        this.borders.push(new Box(this.size, i));
        this.borders.push(new Box(i, -1));
        this.borders.push(new Box(i, this.size));
    }
};




