var KeyPressHandler = function (snake, game) {
    this.game = game;
    this.snake = snake;
    this.opposites = new Map();
    this.opposites
            .set("up", "down")
            .set("down", "up")
            .set("right", "left")
            .set("left", "right");
};
KeyPressHandler.prototype.handleEvent = function (e) {
    switch (e.key) {
        case "ArrowUp":
            this.handleArrow("up");
            break;
        case "ArrowDown":
            this.handleArrow("down");
            break;
        case "ArrowLeft":
            this.handleArrow("left");
            break;
        case "ArrowRight":
            this.handleArrow("right");
            break;
        case "Enter":
            if (this.game.stopped) {
                this.game.stopped = false;
                this.game.play();
            } else {
                this.game.stopped = true;
            }
            break;
        case "Escape":
            this.game.stopped = true;
            setTimeout(this.game.start, 100);
    }
};
KeyPressHandler.prototype.handleArrow = function (direction) {
    var ACCELERANCE = 1.25;
    // if an arrow is pressed in the direction of the snake, speed up
    if (this.snake.direction === direction) {
        this.game.delay /= ACCELERANCE;
    } else if (this.snake.direction !== this.opposites.get(direction)) {
        // cannot turn around 180 degrees
        this.snake.direction = direction;
    } else {
        this.game.delay *= ACCELERANCE;
        // if an arrow in the opposite direction is pressed
        // slow down
    }
};
