var Snake = function (length, startBox) {
    this.snakeBody = [];
    this.length = length;
    this.direction = "right";
    this.createAt(startBox);
};

Snake.prototype.createAt = function (box) {
    this.snakeBody = [];
    var x = box.x;
    var y = box.y;
    for (var i = this.length; i > 0; i--) {
        this.snakeBody.push(new Box(x - this.length + i, y));
    }
    console.log("snake created" + this.toString());
};

Snake.prototype.move = function () {
    // all body parts except the head are replaced by their predecessors
    for (var i = this.snakeBody.length - 1; i > 0; i--) {
        this.snakeBody[i] = this.snakeBody[i - 1];
    }
    // new head is added:
    var newHead = this.getForehead();
    this.snakeBody[0] = newHead;
};

Snake.prototype.head = function () {
    return this.snakeBody[0];
};

Snake.prototype.getForehead = function () {
    var x = this.head().x;
    var y = this.head().y;
    switch (this.direction) {
        case "up":
            y -= 1;
            break;
        case "down":
            y += 1;
            break;
        case "left":
            x -= 1;
            break;
        case "right":
            x += 1;
            break;
    }
    return new Box(x, y);
};

Snake.prototype.toString = function () {
    var result = "";
    for (var i = 0; i < this.snakeBody.length; i++) {
        result += this.snakeBody[i];
    }
    return result;
};

Snake.prototype.grow = function (intensity) {
    var intensity = intensity || 3;
    var snakeBody = this.snakeBody;
    for (var i = 0; i < intensity; i++) {
        snakeBody.push(snakeBody[snakeBody.length - 1]);
    }
    // the body is being prolonged "under" the snake's tail (last box) so that
    // it doesn't collide with any barriers or pills. These hidden parts
    //  of the snake's body are made visible one by one as the snakes moves.
};




