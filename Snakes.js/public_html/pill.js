const colors = new Map();
colors.set(3, "red");
colors.set(4, "green");
colors.set(5, "blue");
colors.set(6, "brown");
colors.set(7, "violet");
colors.set(8, "black");

var Pill = function (x, y, power) {
    this.box = new Box(x, y);
    this.power = power || 3;
};

Pill.prototype.toString = function(){
    return this.box.toString() + "power: " + this.power;
};

Pill.prototype.getColor = function(){
    return colors.get(this.power) || "orange";
};


